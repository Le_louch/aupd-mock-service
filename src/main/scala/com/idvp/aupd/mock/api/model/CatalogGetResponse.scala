package com.idvp.aupd.mock.api.model

import java.util.Date

import com.fasterxml.jackson.annotation.{JsonCreator, JsonProperty}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
case class CatalogGetResponse @JsonCreator()(
                                              @JsonProperty("status") status: Int,
                                              @JsonProperty("message") message: String,
                                              @JsonProperty("response") response: List[CatalogObject],
                                              @JsonProperty("validDate") validDate: Date,
                                              @JsonProperty("filters") filters: Map[String, AnyRef]
                                            )

case class CatalogObject @JsonCreator()(
                                         @JsonProperty("ogv_id") ogvId: String,
                                         @JsonProperty("ogv") ogv: String,
                                         @JsonProperty("create_date") createDate: String,
                                         @JsonProperty("is_deleted") deleted: Int,
                                         @JsonProperty("signature_date") signatureDate: String,
                                         @JsonProperty("system_object_id") systemObjectId: String,
                                         @JsonProperty("global_id") globalId: Long,
                                         @JsonProperty("personnel_num") personelNum: String,
                                         @JsonProperty("surname") surname: String,
                                         @JsonProperty("first_name") firstName: String,
                                         @JsonProperty("second_name") secondName: String,
                                         @JsonProperty("birth_date") birthDate: String,
                                         @JsonProperty("gender") gender: List[Gender],
                                         @JsonProperty("snils_num") snilsNum: String,
                                         @JsonProperty("inn_num") innNum: String,
                                         @JsonProperty("passport") passport: List[Passport],
                                         @JsonProperty("job_exp") jobExp: List[JobExp],
                                         @JsonProperty("internal_mov") internalMov: List[InternalMov]
                                       )

case class Gender @JsonCreator()(@JsonProperty("key") key: String,
                                 @JsonProperty("value") value: String)

case class Passport @JsonCreator()(@JsonProperty("is_deleted") deleted: Int,
                                   @JsonProperty("global_object_id") globalObjectId: Long,
                                   @JsonProperty("global_id") globalId: Long)

case class JobExp @JsonCreator()(@JsonProperty("is_deleted") deleted: Int,
                                 @JsonProperty("global_object_id") globalObjectId: Long,
                                 @JsonProperty("global_id") globalId: Long,
                                 @JsonProperty("code_job_exp") codeJobExp: String,
                                 @JsonProperty("month_count") monthCount: java.lang.Integer,
                                 @JsonProperty("day_count") dayCount: java.lang.Integer,
                                 @JsonProperty("reference_date") birthDate: String)

case class InternalMov @JsonCreator()(@JsonProperty("is_deleted") deleted: Int,
                                      @JsonProperty("global_object_id") globalObjectId: Long,
                                      @JsonProperty("global_id") globalId: Long)