package com.idvp.aupd.mock.api

import java.net.URL

import com.idvp.aupd.mock.api.model.{CurrentUserRolesGettingRequest, CurrentUserRolesGettingResponse, Error}
import com.idvp.aupd.mock.service.AupdMockService
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.Response.Status
import javax.ws.rs.core.{MediaType, NewCookie, Response}
import javax.ws.rs._
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.headers.Header
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse


/**
 * @author olegzinovev
 * @since 03.11.2020
 * */
@Path("/aupd")
@ApplicationScoped
class AupdMockResource(val service: AupdMockService,
                       @ConfigProperty(name = "aupd.redirect.url", defaultValue = "https://admin.dev.idvp.net") val redirectUrl: URL,
                       @ConfigProperty(name = "aupd.cookie.domain", defaultValue = ".dev.idvp.net") val cookieDomain: String) {

  //noinspection QsUndeclaredPathMimeTypesInspection
  @Operation(summary = "Выполняет эмуляцию логина пользователя")
  @APIResponse(description = "Выполняет редирект на адрес backUrl или на адрес модуля iDPV Admin, если backUrl не задан",
    responseCode = "302",
    headers = Array(new Header(name = "Set-Cookie", description = "Устанавливает куку для аутентификации пользователя"))
  )
  @Path("")
  @GET
  def login(@QueryParam("backUrl") backUrl: String): Response = {
    val url = Option(backUrl)
      .filterNot(b => b == null || b.isBlank)
      .map(new URL(_))
      .getOrElse(this.redirectUrl)
    val token = service.getAuthToken
    val path = url.getPath match {
      case x if x == null || x.isEmpty => "/"
      case x => x
    }
    Response.status(Status.FOUND)
      .location(url.toURI)
      .cookie(new NewCookie("aupd_token", token, path, cookieDomain, null, Integer.MAX_VALUE, false))
      .cookie(new NewCookie("obr_id", "1", path, cookieDomain, null, Integer.MAX_VALUE, false))
      .build()
  }


  //noinspection ScalaUnusedSymbol
  @Operation(summary = "Возвращает информацию о ролях пользователя в подсистеме",
    description = "Если в заголовок Authorization передан корректный заголовок безопасности - возвращает данные пользователя. " +
      "Иначе возвращает информацию об ошибке.")
  @Path("v1/getCurrentUserRoles")
  @Produces(Array(MediaType.APPLICATION_JSON))
  @GET
  def getCurrentUserRoles(@QueryParam("obrId") obrId: String,
                          @QueryParam("subsystemId") subsystemId: Int,
                          @HeaderParam("Authorization") auth: String): CurrentUserRolesGettingResponse = {
    val token = service.getAuthToken
    if (auth != s"Bearer $token") {
      CurrentUserRolesGettingResponse(null, 0, List(), null, Set(Error("1", "Unauthorized")))
    } else {
      val r = service.getCurrentUserRoles
      r.copy(requestParameters = CurrentUserRolesGettingRequest(obrId, token, subsystemId))
    }
  }
}
