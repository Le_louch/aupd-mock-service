package com.idvp.aupd.mock.api.model


import com.fasterxml.jackson.annotation.{JsonCreator, JsonProperty}

/**
 * @author olegzinovev
 * @since 03.11.2020
 * */
case class CurrentUserRolesGettingResponse @JsonCreator()(@JsonProperty("currentUserRolesGettingResult") currentUserRolesGettingResult: String,
                                                          @JsonProperty("currentMeshRoleId") currentMeshRoleId: Int,
                                                          @JsonProperty("currentSubsystemRoles") currentSubsystemRoles: List[LocalRoleBrief],
                                                          @JsonProperty("requestParameters") requestParameters: CurrentUserRolesGettingRequest,
                                                          @JsonProperty("errors") errors: Set[Error])

case class LocalRoleBrief @JsonCreator()(@JsonProperty("subsystemRoleId") subsystemRoleId: Long,
                                         @JsonProperty("schoolId") schoolId: String)

case class CurrentUserRolesGettingRequest @JsonCreator()(@JsonProperty("obrId") obrId: String,
                                                         @JsonProperty("meshToken") meshToken: String,
                                                         @JsonProperty("applicationId") applicationId: Int)

case class Error @JsonCreator()(@JsonProperty("errorCode") errorCode: String,
                                @JsonProperty("errorMessage") errorMessage: String)