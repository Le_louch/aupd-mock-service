package com.idvp.aupd.mock.api

import com.idvp.aupd.mock.api.model.{CatalogGetResponse, CurrentUserRolesGettingResponse}
import com.idvp.aupd.mock.service.{AupdMockService, Nsi3MockService}
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.MediaType
import javax.ws.rs.{Consumes, GET, POST, Path, Produces}
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
@Path("/settings")
@ApplicationScoped
class SettingsResource(val aupdService: AupdMockService,
                       val nsi3Service: Nsi3MockService) {

  @Operation(summary = "Возвращает текущий токен аутетифиации")
  @Path("aupd/auth-token")
  @Produces(Array(MediaType.TEXT_PLAIN))
  @GET
  def getAuthToken: String = aupdService.getAuthToken

  @Operation(summary = "Устанавливает текущий токен аутетифиации",
    description = "Получить токен можно с помощью сайта jwt.io. Тип подписи - RS256, в PAYLOAD должны быть поля sub и stf")
  @Path("aupd/auth-token")
  @Produces(Array(MediaType.TEXT_PLAIN))
  @Consumes(Array(MediaType.TEXT_PLAIN))
  @POST
  def setAuthToken(@RequestBody value: String): Unit = aupdService.saveAuthToken(value)

  @Operation(summary = "Возвращает информацию о текущих ролях пользователя в подсистеме")
  @Path("aupd/current-user-roles")
  @Produces(Array(MediaType.APPLICATION_JSON))
  @GET
  def getCurrentUserRoles: CurrentUserRolesGettingResponse = aupdService.getCurrentUserRoles

  @Operation(summary = "Устанавливает информацию о текущих ролях пользователя в подсистеме")
  @Path("aupd/current-user-roles")
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.TEXT_PLAIN))
  @POST
  def setCurrentUserRoles(@RequestBody value : CurrentUserRolesGettingResponse): Unit = aupdService.saveCurrentUserRoles(value)

  @Operation(summary = "Возвращает информацию о пользователе из каталога")
  @Path("nsi3/catalog/get")
  @Produces(Array(MediaType.APPLICATION_JSON))
  @GET
  def getUserCatalog: CatalogGetResponse = nsi3Service.catalogGet

  @Operation(summary = "Устанавливает информацию о пользователе из каталога")
  @Path("aupd/catalog/get")
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.TEXT_PLAIN))
  @POST
  def setUserCatalog(@RequestBody value : CatalogGetResponse): Unit = nsi3Service.saveCatalogGet(value)

}
