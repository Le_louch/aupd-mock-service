package com.idvp.aupd.mock.api

import com.idvp.aupd.mock.api.model.CatalogGetResponse
import com.idvp.aupd.mock.service.Nsi3MockService
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs._
import javax.ws.rs.core.MediaType
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody


/**
 * @author olegzinovev
 * @since 03.11.2020
 * */
@Path("/nsi3")
@ApplicationScoped
class Nsi3MockResource(val service: Nsi3MockService) {

  //noinspection ScalaUnusedSymbol
  @Operation(summary = "Возвращает описание структуры каталога",
    description = "В MOCK сервисе возвращает пустой JSON, так как этот метод используется только для проверки соедиения")
  @Path("catalog/list")
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @POST
  def catalogList(@RequestBody(description = "Параметр запроса игнорируется", content = Array(new Content(example = "{}")))
                  filter: Map[String, AnyRef]): Map[String, Any] =
    service.catalogList

  //noinspection ScalaUnusedSymbol
  @Operation(summary = "Возвращает информацию о пользователе из каталога")
  @Path("catalog/get")
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @POST
  def catalogGet(@RequestBody(description = "Параметр запроса игнорируется", content = Array(new Content(example = "{}")))
                 filter: Map[String, AnyRef]): CatalogGetResponse =
    service.catalogGet
}
