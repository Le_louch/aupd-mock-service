package com.idvp.aupd.mock.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.quarkus.jackson.ObjectMapperCustomizer
import javax.enterprise.context.ApplicationScoped

/**
 * @author olegzinovev
 * @since 04.11.2020
 * */
@ApplicationScoped
class JacksonConfig extends ObjectMapperCustomizer {
  override def customize(objectMapper: ObjectMapper): Unit = objectMapper.registerModule(DefaultScalaModule)
}
