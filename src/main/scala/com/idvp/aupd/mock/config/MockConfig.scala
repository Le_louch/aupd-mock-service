package com.idvp.aupd.mock.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.idvp.aupd.mock.service.{AupdMockService, AupdMockServiceImpl, Nsi3MockService, Nsi3MockServiceImpl}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces
import org.eclipse.microprofile.config.inject.ConfigProperty

/**
 * @author olegzinovev
 * @since 04.11.2020
 * */
@ApplicationScoped
class MockConfig(val mapper: ObjectMapper,
                 @ConfigProperty(name = "aupd.store.path", defaultValue = " ") val path: String) {

  @Produces
  @ApplicationScoped
  def createAupdMockService(): AupdMockService = {
    val p = if (path == null || path.isEmpty || path.isBlank) {
      System.getProperty("java.io.tmpdir")
    } else {
      path
    }

    new AupdMockServiceImpl(mapper, p)
  }


  @Produces
  @ApplicationScoped
  def createNsi3MockService(): Nsi3MockService = {
    val p = if (path == null || path.isEmpty || path.isBlank) {
      System.getProperty("java.io.tmpdir")
    } else {
      path
    }

    new Nsi3MockServiceImpl(mapper, p)
  }
}
