package com.idvp.aupd.mock.service

import com.idvp.aupd.mock.api.model.CurrentUserRolesGettingResponse

/**
 * @author olegzinovev
 * @since 03.11.2020
 * */
trait AupdMockService {
  def getAuthToken: String

  def saveAuthToken(token: String): Unit

  def getCurrentUserRoles: CurrentUserRolesGettingResponse

  def saveCurrentUserRoles(r: CurrentUserRolesGettingResponse): Unit
}
