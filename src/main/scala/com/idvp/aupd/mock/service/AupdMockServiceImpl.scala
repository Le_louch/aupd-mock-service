package com.idvp.aupd.mock.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.idvp.aupd.mock.api.model.{CurrentUserRolesGettingRequest, CurrentUserRolesGettingResponse, LocalRoleBrief}

/**
 * @author olegzinovev
 * @since 03.11.2020
 * */
class AupdMockServiceImpl(mapper: ObjectMapper,
                          path: String) extends AbstractServiceImpl(mapper, path) with AupdMockService {

  override def getAuthToken: String = read[String]("auth-token.json")
    .getOrElse(AupdMockServiceImpl.authToken)

  override def saveAuthToken(token: String): Unit = write("auth-token.json", token)

  override def getCurrentUserRoles: CurrentUserRolesGettingResponse =
    read[CurrentUserRolesGettingResponse]("current-user-roles.json")
      .getOrElse(AupdMockServiceImpl.currentUserRoles)

  override def saveCurrentUserRoles(r: CurrentUserRolesGettingResponse): Unit = write("current-user-roles.json", r)

}

//noinspection ScalaUnusedSymbol
private[service] object AupdMockServiceImpl {
  private[service] val currentUserRoles = CurrentUserRolesGettingResponse(
    currentUserRolesGettingResult = "OK",
    currentMeshRoleId = 1,
    currentSubsystemRoles = List(LocalRoleBrief(1, "#1"), LocalRoleBrief(2, "#2")),
    requestParameters = CurrentUserRolesGettingRequest(null, null, 0),
    errors = Set()
  )

  private[service] val authToken =
    """eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwic3RmIjoiMSJ9.PlO6h-4WC-GvXWysQCJx9XClUB5_CNmT0TwwMZhanspql5S3823zdr-tNgM9WmeFGoH3fy8zFpJP2J1GeuUozAQpcj3gx0CP-Mt2IDex_R6JV-W6HfNqMI5syyVYhtgcgtlqh5RIdwYzOjPImMKsyNjTNTU4ScmqzHBlC8IyLvi5r8AyT1S0M6WVimd9tzDf8WgbvEYiwrTGI3fVXaxlzhHJx4Zs-laKFSYiluzihcd9D-76d2T4fVW_w5ZK9OkKHQh3BZOGwnMPqB6EGtYVo9GHUrFlsVNFCKB1ZCHV5igZIwzzef3v-Jj4T_-fvNvVqEYw7n_sGPA-J7gsMNgYUw"""

  private val publicKey =
    """-----BEGIN PUBLIC KEY-----
      |MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv
      |vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc
      |aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy
      |tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0
      |e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb
      |V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9
      |MwIDAQAB
      |-----END PUBLIC KEY-----""".stripMargin

  private val privateKey =
    """-----BEGIN RSA PRIVATE KEY-----
      |MIIEogIBAAKCAQEAnzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA+kzeVOVpVWw
      |kWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr/Mr
      |m/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEi
      |NQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e+lf4s4OxQawWD79J9/5d3Ry0vbV
      |3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa+GSYOD2
      |QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9MwIDAQABAoIBACiARq2wkltjtcjs
      |kFvZ7w1JAORHbEufEO1Eu27zOIlqbgyAcAl7q+/1bip4Z/x1IVES84/yTaM8p0go
      |amMhvgry/mS8vNi1BN2SAZEnb/7xSxbflb70bX9RHLJqKnp5GZe2jexw+wyXlwaM
      |+bclUCrh9e1ltH7IvUrRrQnFJfh+is1fRon9Co9Li0GwoN0x0byrrngU8Ak3Y6D9
      |D8GjQA4Elm94ST3izJv8iCOLSDBmzsPsXfcCUZfmTfZ5DbUDMbMxRnSo3nQeoKGC
      |0Lj9FkWcfmLcpGlSXTO+Ww1L7EGq+PT3NtRae1FZPwjddQ1/4V905kyQFLamAA5Y
      |lSpE2wkCgYEAy1OPLQcZt4NQnQzPz2SBJqQN2P5u3vXl+zNVKP8w4eBv0vWuJJF+
      |hkGNnSxXQrTkvDOIUddSKOzHHgSg4nY6K02ecyT0PPm/UZvtRpWrnBjcEVtHEJNp
      |bU9pLD5iZ0J9sbzPU/LxPmuAP2Bs8JmTn6aFRspFrP7W0s1Nmk2jsm0CgYEAyH0X
      |+jpoqxj4efZfkUrg5GbSEhf+dZglf0tTOA5bVg8IYwtmNk/pniLG/zI7c+GlTc9B
      |BwfMr59EzBq/eFMI7+LgXaVUsM/sS4Ry+yeK6SJx/otIMWtDfqxsLD8CPMCRvecC
      |2Pip4uSgrl0MOebl9XKp57GoaUWRWRHqwV4Y6h8CgYAZhI4mh4qZtnhKjY4TKDjx
      |QYufXSdLAi9v3FxmvchDwOgn4L+PRVdMwDNms2bsL0m5uPn104EzM6w1vzz1zwKz
      |5pTpPI0OjgWN13Tq8+PKvm/4Ga2MjgOgPWQkslulO/oMcXbPwWC3hcRdr9tcQtn9
      |Imf9n2spL/6EDFId+Hp/7QKBgAqlWdiXsWckdE1Fn91/NGHsc8syKvjjk1onDcw0
      |NvVi5vcba9oGdElJX3e9mxqUKMrw7msJJv1MX8LWyMQC5L6YNYHDfbPF1q5L4i8j
      |8mRex97UVokJQRRA452V2vCO6S5ETgpnad36de3MUxHgCOX3qL382Qx9/THVmbma
      |3YfRAoGAUxL/Eu5yvMK8SAt/dJK6FedngcM3JEFNplmtLYVLWhkIlNRGDwkg3I5K
      |y18Ae9n7dHVueyslrb6weq7dTkYDi3iOYRW8HRkIQh06wEdbxt0shTzAJvvCQfrB
      |jg/3747WSsf/zBTcHihTRBdAv6OmdhV4/dD5YBfLAkLrd+mX7iE=
      |-----END RSA PRIVATE KEY-----""".stripMargin
}
