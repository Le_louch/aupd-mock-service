package com.idvp.aupd.mock.service

import java.nio.file.{AtomicMoveNotSupportedException, Files, Paths, StandardCopyOption, StandardOpenOption}

import com.fasterxml.jackson.databind.ObjectMapper
import com.idvp.aupd.mock.utils.AutoCloseableUtils._

import scala.reflect.ClassTag

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
protected abstract class AbstractServiceImpl(val mapper: ObjectMapper,
                                             val path: String) {

  protected def read[T](file: String)(implicit tag: ClassTag[T]): Option[T] = {
    val path = Paths.get(this.path).resolve(file)
    if (Files.exists(path)) {
      Some(Files.newInputStream(path, StandardOpenOption.READ)
        .use {
          mapper.readValue(_, tag.runtimeClass.asInstanceOf[Class[T]])
        })
    } else {
      None
    }
  }

  protected def write(file: String, value: AnyRef): Unit = {
    val path = Paths.get(this.path).resolve(file)
    Files.createDirectories(path.getParent)
    val tempPath = Files.createTempFile(path.getParent, null, null)

    Files.newOutputStream(tempPath, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
      .use({
        mapper.writeValue(_, value)
      })

    try {
      Files.move(tempPath, path, StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING)
    } catch {
      case _: AtomicMoveNotSupportedException =>
        Files.move(tempPath, path, StandardCopyOption.REPLACE_EXISTING)
    }
  }
}
