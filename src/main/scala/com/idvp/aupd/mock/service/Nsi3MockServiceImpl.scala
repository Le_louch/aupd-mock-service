package com.idvp.aupd.mock.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.idvp.aupd.mock.api.model.{CatalogGetResponse, CatalogObject, Gender, InternalMov, JobExp, Passport}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
class Nsi3MockServiceImpl(mapper: ObjectMapper,
                          path: String) extends AbstractServiceImpl(mapper, path) with Nsi3MockService {
  override def catalogList: Map[String, Any] = Map()

  override def catalogGet: CatalogGetResponse =
    read[CatalogGetResponse]("catalog-get-response.json")
      .getOrElse(Nsi3MockServiceImpl.catalogGetResponse)

  override def saveCatalogGet(value: CatalogGetResponse): Unit =
    write("catalog-get-response.json", value)
}

private[service] object Nsi3MockServiceImpl {
  private[service] val catalogGetResponse = CatalogGetResponse(
    0,
    "",
    List(CatalogObject(
      "8778",
      "Системные пользователи",
      "19.09.2019 09:46:25",
      0,
      "11.12.2019 12:16:47",
      "9067",
      31238496,
      "9067",
      "Голай",
      "Ирина",
      "Ахмедовна",
      "31.12.1989",
      List(Gender("2", "Женский")),
      "101-101-101 25",
      "773773773800",
      List(Passport(
        0,
        31238496,
        11827
      )),
      List(JobExp(
        0,
        31238496,
        19067,
        "00002",
        null,
        null,
        "26.08.2001"
      )),
      List(InternalMov(
        0,
        31238496,
        47280
      ), InternalMov(
        0,
        31238496,
        47281
      ), InternalMov(
        0,
        31238496,
        47282
      ), InternalMov(
        0,
        31238496,
        47283
      ), InternalMov(
        0,
        31238496,
        47284
      ), InternalMov(
        0,
        31238496,
        47285
      ), InternalMov(
        0,
        31238496,
        47286
      ), InternalMov(
        0,
        31238496,
        47287
      ), InternalMov(
        0,
        31238496,
        47288
      ), InternalMov(
        0,
        31238496,
        47289
      ), InternalMov(
        0,
        31238496,
        47290
      ), InternalMov(
        0,
        31238496,
        47291
      ))
    )),
    null,
    Map()
  )
}
