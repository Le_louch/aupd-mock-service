package com.idvp.aupd.mock.service

import com.idvp.aupd.mock.api.model.CatalogGetResponse

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
trait Nsi3MockService {
  def catalogList: Map[String, Any]
  def catalogGet: CatalogGetResponse
  def saveCatalogGet(value: CatalogGetResponse): Unit
}
