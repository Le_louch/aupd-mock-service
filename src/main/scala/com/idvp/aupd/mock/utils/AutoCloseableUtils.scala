package com.idvp.aupd.mock.utils

import scala.util.control.NonFatal

/**
 * @author olegzinovev
 * @since 04.11.2020
 * */
object AutoCloseableUtils {

  implicit class Raii[T <: AutoCloseable](val resource: T) {
    def use[V](f: T => V): V = {
      require(resource != null, "resource is null")
      var exception: Throwable = null
      try {
        f(resource)
      } catch {
        case NonFatal(e) =>
          exception = e
          throw e
      } finally {
        closeAndAddSuppressed(exception, resource)
      }
    }

    private def closeAndAddSuppressed(e: Throwable,
                                      resource: AutoCloseable): Unit = {
      if (e != null) {
        try {
          resource.close()
        } catch {
          case NonFatal(suppressed) =>
            e.addSuppressed(suppressed)
        }
      } else {
        resource.close()
      }
    }

  }

}
