package com.idvp.aupd.mock.api.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.junit.jupiter.api.{Assertions, Test}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
class CurrentUserRolesGettingResponseSerDeTest {
  @Test
  def testSerDe(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val ser = objectMapper.writeValueAsString(currentUserRoles)
    val de = objectMapper.readValue(ser, classOf[CurrentUserRolesGettingResponse])
    Assertions.assertEquals(currentUserRoles, de)
  }

  private val currentUserRoles = CurrentUserRolesGettingResponse(
    currentUserRolesGettingResult = "OK",
    currentMeshRoleId = 1,
    currentSubsystemRoles = List(LocalRoleBrief(1, "#1"), LocalRoleBrief(2, "#2")),
    requestParameters = CurrentUserRolesGettingRequest(null, null, 0),
    errors = Set(Error("1", "Test"))
  )
}
