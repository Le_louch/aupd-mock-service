package com.idvp.aupd.mock.api.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.junit.jupiter.api.{Assertions, Test}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
class CatalogGetResponseSerDeTest {
  @Test
  def testSerDe(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val ser = objectMapper.writeValueAsString(catalogGetResponse)
    val de = objectMapper.readValue(ser, classOf[CatalogGetResponse])
    Assertions.assertEquals(catalogGetResponse, de)
  }

  private val catalogGetResponse = CatalogGetResponse(
    0,
    "",
    List(CatalogObject(
      "8778",
      "Системные пользователи",
      "19.09.2019 09:46:25",
      0,
      "11.12.2019 12:16:47",
      "9067",
      31238496,
      "9067",
      "Голайченко",
      "Ирина",
      "Алексеевна",
      "31.08.1984",
      List(Gender("2", "Женский")),
      "101-565-288 25",
      "773170315800",
      List(Passport(
        0,
        31238496,
        11827
      )),
      List(JobExp(
        0,
        31238496,
        19067,
        "00002",
        null,
        null,
        "26.08.2001"
      )),
      List(InternalMov(
        0,
        31238496,
        47280
      ), InternalMov(
        0,
        31238496,
        47281
      ), InternalMov(
        0,
        31238496,
        47282
      ), InternalMov(
        0,
        31238496,
        47283
      ), InternalMov(
        0,
        31238496,
        47284
      ), InternalMov(
        0,
        31238496,
        47285
      ), InternalMov(
        0,
        31238496,
        47286
      ), InternalMov(
        0,
        31238496,
        47287
      ), InternalMov(
        0,
        31238496,
        47288
      ), InternalMov(
        0,
        31238496,
        47289
      ), InternalMov(
        0,
        31238496,
        47290
      ), InternalMov(
        0,
        31238496,
        47291
      ))
    )),
    null,
    Map()
  )
}
