package com.idvp.aupd.mock.service

import java.nio.file.Files

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.{Assertions, Test}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
class AupdMockServiceTest {
  @Test
  def testGetAuthToken(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new AupdMockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    val token = service.getAuthToken
    Assertions.assertEquals(AupdMockServiceImpl.authToken, token)
  }

  @Test
  def testSaveAuthToken(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new AupdMockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    service.saveAuthToken("1")
    val token = service.getAuthToken
    Assertions.assertEquals("1", token)
  }

  @Test
  def testGetCurrentUserRoles(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new AupdMockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    val roles = service.getCurrentUserRoles
    Assertions.assertEquals(AupdMockServiceImpl.currentUserRoles, roles)
  }

  @Test
  def testSaveCurrentUserRoles(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new AupdMockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    val newRoles = AupdMockServiceImpl.currentUserRoles.copy(currentMeshRoleId = 100500)
    service.saveCurrentUserRoles(newRoles)
    val roles = service.getCurrentUserRoles
    Assertions.assertEquals(newRoles, roles)
  }
}
