package com.idvp.aupd.mock.service

import java.nio.file.Files

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.junit.jupiter.api.{Assertions, Test}

/**
 * @author olegzinovev
 * @since 05.11.2020
 * */
class Nsi3MockServiceTest {

  @Test
  def testCatalogList(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new Nsi3MockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    val list = service.catalogList
    Assertions.assertEquals(Map(), list)
  }

  @Test
  def testCatalogGet(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new Nsi3MockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    val user = service.catalogGet
    Assertions.assertEquals(Nsi3MockServiceImpl.catalogGetResponse, user)
  }

  @Test
  def testSaveCatalogGet(): Unit = {
    val objectMapper = new ObjectMapper().registerModule(DefaultScalaModule)
    val service = new Nsi3MockServiceImpl(objectMapper, Files.createTempDirectory(null).toAbsolutePath.toString)
    service.saveCatalogGet(Nsi3MockServiceImpl.catalogGetResponse.copy(response = List()))
    val user = service.catalogGet
    Assertions.assertEquals(Nsi3MockServiceImpl.catalogGetResponse.copy(response = List()), user)
  }
}
